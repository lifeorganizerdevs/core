<?php

namespace LifeOrganizer\Core\Category\Exception;

use Exception;

final class CategoryDoesNotExist extends Exception
{
}
